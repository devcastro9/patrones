﻿using Microsoft.EntityFrameworkCore;
using System;
using Patrones.Data;
using Patrones.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Patrones
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            using (var context = new AppDbContext())
            {
                List<Cliente> result = await context.Clientes.AsNoTracking().ToListAsync();
                foreach (var item in result)
                {
                    Console.WriteLine(item.Nombres + " " + item.ApPaterno + " " + item.ApMaterno);
                }
            }
        }
    }
}